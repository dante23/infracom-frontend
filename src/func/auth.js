import axios from "axios";

export const createOrUpdateUser = async (authtoken) => {
    const data = {}
    const config = {
        headers: {
            authtoken
        }
    }
    return await axios.post(`${process.env.REACT_APP_API}/create-update-user`, data, config)
}
