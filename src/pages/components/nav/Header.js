 import React, { useState } from "react";
 import { Menu } from "antd";
 import {
     UserOutlined,
     UserAddOutlined,
     AppstoreOutlined,
     SettingOutlined,
     LogoutOutlined,
 } from '@ant-design/icons';

 import { Link } from "react-router-dom";
 import firebase from "firebase";
 import {useDispatch, useSelector} from "react-redux";
 import { useHistory } from "react-router-dom";


const { SubMenu, Item } = Menu;

const Header = () => {
    const [
        current,
        setCurrent,
    ] = useState("home");
    let dispatch = useDispatch()
    let history = useHistory()
    let {user} = useSelector((state) => ( { ...state } ))

    const handleClick = (e) => {
        //
        //console.log(e.key)
        setCurrent(e.key)
    };

    const logout = () => {
        firebase.auth().signOut();
        dispatch({
            type: "LOGOUT",
            payload: null
        })
        //setCurrent("login")
        history.push("/login")
    }

    return (
        <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal">
            <Item key="home" icon={<AppstoreOutlined />}>
                <Link to="/">Home</Link>
            </Item>
            {user && (
                <SubMenu key="SubMenu"
                         icon={<SettingOutlined />}
                         title={user.email && user.email.split("@")[0]}
                         className="float-right"
                >
                    <Item key="setting:1">Options 1</Item>
                    <Item key="setting:2">Options 2</Item>
                    <Item icon={<LogoutOutlined />} onClick={logout}>Logout</Item>
                </SubMenu>
            )}

            {!user && (
                <>
                <Item key="register" icon={<UserAddOutlined />} className="float-right">
                    <Link to="/register">Register</Link>
                </Item>
                <Item key="login" icon={<UserOutlined />} className="float-right">
                <Link to="/login">Login</Link>
                </Item>
                </>
            )}

        </Menu>
    );
}

export default Header
