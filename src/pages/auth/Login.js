import React, { useState, useEffect } from "react"
import { Button } from "antd";
import { auth, googleAuthProvider } from "../../firebase";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import {
    MailOutlined,
    GoogleOutlined,
} from '@ant-design/icons';
import {Link} from "react-router-dom";
import { createOrUpdateUser } from '../../func/auth';

const Login = ({history}) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch();

    const {user} = useSelector((result) => ({...result} ))

    useEffect(() => {
        if (user && user.token) history.push('/')
    },[user])

    const loginSubmit = async (e) => {
        e.preventDefault();
        setLoading(true)
        try {
            //send data to firebase
            const result = await auth.signInWithEmailAndPassword(email, password)

            const { user } = result
            const IdTokenResult = await user.getIdTokenResult()
            await createOrUpdateUser(IdTokenResult.token)
                .then((res) => {
                    console.log("success send data to backend", res)
                    dispatch({
                        type: "LOGGED_IN_USER",
                        payload: {
                            name: user.name,
                            email: user.email,
                            role: user.role,
                            token: IdTokenResult.token
                        }
                    })

                } )
                .catch((err) => console.log("error send data to backend", err))

            // redirect into login after success login
            history.push("/")
            toast.success("Successfully login.")

        } catch (e) {
            toast.error(e.message)
            setLoading(false)
        }


    }
    
    const googleLogin = async  () => {
            auth.signInWithPopup(googleAuthProvider)
                .then(async (result) => {
                    const { user } = result
                    const IdTokenResult = await user.getIdTokenResult()
                    dispatch({
                        type: "LOGGED_IN_USER",
                        payload: {
                            email: user.email,
                            token: IdTokenResult.token
                        }
                    })
                    // redirect into login after success login
                    history.push("/")
                    toast.success("Successfully login.")
                })
                .catch((e) => toast.error(e.message))
    }
    const loginForm = () => <form onSubmit={loginSubmit}>
        <div className="form-group">
            <input
                type="email"
                className="form-control"
                onChange={ (e) => setEmail(e.target.value) }
                value={email}
                placeholder="your email"
                autoFocus
            />
        </div>

        <div className="form-group">
            <input
                type="password"
                className="form-control"
                onChange={ (e) => setPassword(e.target.value) }
                value={password}
                placeholder="your password"
            />
        </div>

        <br />
            <Button type="primary"
                    className="mb-3"
                    block
                    shape="round"
                    size="large"
                    icon={<MailOutlined />}
                    disabled={!email | password.length < 6}
                    htmlType="submit"
            >
                Login with email/password
            </Button>


    </form>

    const buttonGoogleLogin = () =>
        <Button type="danger"
                className="mb-3"
                block
                shape="round"
                size="large"
                icon={<GoogleOutlined />}
                onClick={googleLogin}
        >
            Login with Google
        </Button>

    const forgotPassword = () => <Link to="/forgot/password" className="text-danger float-right">Forgot Password?</Link>

    return (
        <div className="container p-5">
            <div className="col-md-5 offset-md-3">
                {loading ? (
                    <h3 className="text-danger">Loading...</h3>
                ) : (
                    <h3>Login</h3>)
                }
                {loginForm()}
                {buttonGoogleLogin()}
                {forgotPassword()}
            </div>
        </div>
    )
}

export default Login;
