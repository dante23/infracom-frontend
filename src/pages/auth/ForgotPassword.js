import React, { useState, useEffect } from 'react'
import { auth } from "../../firebase";
import { toast } from "react-toastify";
import { useSelector } from "react-redux";


const ForgotPassword = ({history}) => {
    const [email, setEmail] = useState("")
    const [loading, setLoading] = useState(false)

    const {user} = useSelector((result) => ({...result} ))

    useEffect(() => {
        if (user && user.token) history.push('/')
    },[user])

    const handleSubmit = async (e) => {
        e.preventDefault()
        setLoading(true)

        const config = {
            url: process.env.REACT_APP_FORGOT_PASSWORD_REDIRECT_URL,
            handleCodeInApp: true,
        }

        await auth.sendPasswordResetEmail(email,config)
            .then((res) => {
                setEmail("")
                toast.success("check your email for reset your password")
                setLoading(false)
            })
            .catch((err) => {
                toast.error(err.message)
                setLoading(false)
            })

    }

    const formForgotPassword = () => <form onSubmit={handleSubmit}>
        <input
            type="email"
            className="form-control"
            onChange={ (e) => setEmail(e.target.value) }
            value={email}
            placeholder="your email"
            autoFocus
        />
        <br />
        <button type="submit" className="btn btn-raised">Submit</button>

    </form>

    return (
        <div className="container col-md-6 offset-md-3 p-5">
            {loading ? (
                <h3 className="text-danger">Loading</h3>
            ) : (
                <h3>Forgot Password</h3>
            )}
            {formForgotPassword()}
        </div>
    )
}

export default ForgotPassword
