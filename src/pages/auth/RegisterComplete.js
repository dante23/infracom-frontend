import React, { useState, useEffect } from "react";
import { auth } from "../../firebase";
import { toast } from "react-toastify";
import {useSelector} from "react-redux";

const RegisterComplete = ({history}) => {
    const  [email, setEmail] = useState("");
    const  [password, setPassword] = useState("");

    const {user} = useSelector((result) => ({...result} ))

    useEffect(() => {
        if (user && user.token) {
            history.push('/')
        }
        setEmail(window.localStorage.getItem(process.env.REACT_APP_LOCALSTORAGE_REGISTRATION_VAR)) 
    }, [user])

    const handleSubmit = async (e) => {
        e.preventDefault();
        
        try {
            const result = await auth.signInWithEmailLink(email, window.location.href)
        

            if ( result.user.emailVerified ) {
                // remove email from localstorage
                window.localStorage.removeItem(process.env.REACT_APP_LOCALSTORAGE_REGISTRATION_VAR)
                //get user id token
                let user = auth.currentUser
                await user.updatePassword(password)
                //const idTokenResult = await user.getIdTokenResult()

                // redux store
                // console.log('user', user, 'idTokenResult', idTokenResult)
                toast.success(`Congrats your registration is successfull, waiting for redirect into home page`)

                //redirect
                history.push('/');

            }
        } catch (error) {
            toast.error(error.message)
        }

    }
    const registerCompleteForm = () => <form onSubmit={handleSubmit}>
        <input
            type="email"
            className="form-control"
            value={email}
            disabled
        />
        <input
            type="password"
            className="form-control"
            onChange={ (e) => setPassword(e.target.value) }
            value={password}
            placeholder="Password"
            autoFocus
        />
        <button type="submit" className="btn btn-raised">Complete Registration</button>
    </form>

    return (
        <div className="container p-5">
            <div className="col-md-5 offset-md-3">
                <h3>Register Complete</h3>
                {registerCompleteForm()}
            </div>
        </div>
    )
}

export default RegisterComplete;
