import React, { useState, useEffect } from "react";
import { auth } from "../../firebase";
import { toast } from "react-toastify";
import {useSelector} from "react-redux";

const Register = ({history}) => {
    const  [email, setEmail] = useState("");

    const {user} = useSelector((result) => ({...result} ))

    useEffect(() => {
        if (user && user.token) history.push('/')
    },[user])

    const handleSubmit = async (e) => {
        e.preventDefault();
        
        // config sign into google email
        const config = {
            url: process.env.REACT_APP_REGISTER_REDIRECT_URL,
            handleCodeInApp: true,
        }

        await auth.sendSignInLinkToEmail(email, config)
            .then(async (result) => {
                // notif
                toast.success(`Email is sent to ${email}. click the link to complete registration.`)

                // save user email in localStorage
                window.localStorage.setItem(process.env.REACT_APP_LOCALSTORAGE_REGISTRATION_VAR, email)

                //clear state
                setEmail("")
            })
            .catch((e) => {
                toast.error(e.message)
            });
    }
    const registerForm = () => <form onSubmit={handleSubmit}>
        <input
            type="email"
            className="form-control"
            onChange={ (e) => setEmail(e.target.value) }
            value={email}
            placeholder="your email"
            autoFocus
        />
        <br />
        <button type="submit" className="btn btn-raised">Register</button>
    </form>

    return (
        <div className="container p-5">
            <div className="col-md-5 offset-md-3">
                <h3>Register</h3>
                {registerForm()}
            </div>
        </div>
    )
}

export default Register;
