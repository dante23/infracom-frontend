import firebase from "firebase";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyA5UD63q7eXpa3vmCpZeovNDJX8xgDl1DM",
    authDomain: "ecommerce-2d0a6.firebaseapp.com",
    databaseURL: "https://ecommerce-2d0a6.firebaseio.com",
    projectId: "ecommerce-2d0a6",
    storageBucket: "ecommerce-2d0a6.appspot.com",
    messagingSenderId: "207027578090",
    appId: "1:207027578090:web:ad62680c75dec76d0de393"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

//export
export const auth = firebase.auth();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
